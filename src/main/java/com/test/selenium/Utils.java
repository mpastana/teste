package com.test.selenium;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;


public class Utils 
{
	public static WebDriver driver;
	@SuppressWarnings("rawtypes")
	public static void startDriver(String browser, DesiredCapabilities cap, String remoteAdress) throws MalformedURLException
	{
		switch (browser) {
		case "Chrome":
			driver = new ChromeDriver(cap);
			break;
		case "Appium":
			driver = new AppiumDriver(new URL(remoteAdress), cap);
		default:
			break;
		}
		
	}	
	public static void startDriver(String browser)
	{
		switch (browser) {
		case "Chrome":
			driver = new ChromeDriver();
			break;
		}
		
	}
    public static void takeScreenshot(String evidenceName) throws IOException
    {
    	File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    	FileUtils.copyFile(scrFile, new File(evidenceName));
    }
	public static boolean isElementPresent(By element)
	{
		
		try {
			driver.findElement(element);
		} catch (NoSuchElementException e) {
			return false;
		}
		return true;
	}
	public static void quitDriver() {
		driver.quit();
		
	}
	@SuppressWarnings("static-access")
	public static boolean waitForElementPresent(By element, int time) throws InterruptedException {
		for (int i = 0; i < time; i++) {
			try {
				driver.findElement(element);
				return true;
			} catch (NoSuchElementException e) {
				Thread.currentThread().sleep(1000);
			}		
		}
		return false;

	}
	@SuppressWarnings("static-access")
	public static boolean waitForElementTextEquals(By element, String result, int time) throws InterruptedException {
		for (int i = 0; i < time; i++) {
			if (driver.findElement(element).getText().equals(result))
				return true;	
			Thread.currentThread().sleep(1000);
		}
		return false;

	}
}
