package com.test.selenium;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.Test;
import org.junit.runner.JUnitCore;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
@Before
	public void beforeTest() throws Exception
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("platformName", "Android");
		cap.setCapability("deviceName", "127.0.0.1:6555");
		cap.setCapability("appPackage", "com.devplank.masterplaca");
		cap.setCapability("appActivity", "com.devplank.masterplaca.MainActivity");
		Utils.startDriver("Appium", cap, "http://127.0.0.1:4723/wd/hub");
		
	}
@Test
	public void desafioCucumber1()
	{
		JUnitCore junit = new JUnitCore();
		junit.run(com.cucumberTest.TestRunnerValida.class);
	}
@Test
	public void desafioCucumber2()
	{
		JUnitCore junit = new JUnitCore();
		junit.run(com.cucumberTest.TestRunnerInvalida.class);
	}
@After
	public void finishDriver()
	{
		Utils.quitDriver();
	}


}
