package com.test.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;

import com.apiTest.ApiLibrary;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ApiTest 
{
	ApiLibrary api;
@Before
	public void beforeTest() throws Exception
	{
		api = new ApiLibrary();
	}
@Test
	public void apiTest1() throws InterruptedException, ClientProtocolException, IOException
	{
		api.getDataFrom("http://services.groupkt.com/country/get/all");
		int num = api.countElements();
		assertEquals(249, num);
	}
@Test
	public void apiTest2() throws InterruptedException, ClientProtocolException, IOException
	{
		api.getDataFrom("http://services.groupkt.com/country/get/all");
		boolean result = api.IsElementPresent("Albania");
		assertTrue(result);
		result = api.IsElementPresent("NotACountry");
		assertFalse(result);
		
	}
@Test
	public void apiTest3() throws InterruptedException, ClientProtocolException, IOException
	{
		api.setBaseUrl("http://services.groupkt.com/state/search/");
		api.setCountry("USA");
		api.setText("New%20York");
		api.setData();
		String result = api.returnData();
		assertEquals("{\n" + 
				"  \"RestResponse\" : {\n" + 
				"    \"messages\" : [ \"Total [1] records found.\" ],\n" + 
				"    \"result\" : [ {\n" + 
				"      \"id\" : 32,\n" + 
				"      \"country\" : \"USA\",\n" + 
				"      \"name\" : \"New York\",\n" + 
				"      \"abbr\" : \"NY\",\n" + 
				"      \"area\" : \"141297SKM\",\n" + 
				"      \"largest_city\" : \"New York City\",\n" + 
				"      \"capital\" : \"Albany\"\n" + 
				"    } ]\n" + 
				"  }\n" + 
				"}", result);
		
	}
}
