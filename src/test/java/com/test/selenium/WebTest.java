package com.test.selenium;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;

import com.selenium.pages.OlxPages;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class WebTest 
{
	private OlxPages page = new OlxPages();
@Before
	public void beforeTest() throws Exception
	{
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\mpastana\\FTESTES\\efa3\\branches\\MP\\driver\\chromedriver.exe");
		Utils.startDriver("Chrome");
		
	}
@Test
	public void desafioSeleniumWeb() throws InterruptedException
	{
	page.login();
	page.searchItem("notebook");
	String result = page.searchItems(5);
	assertEquals("Encontrado todos os items da lista", result);
	}

@Test
public void desafioSeleniumWeb2() throws InterruptedException, IOException
{
	page.login();
	page.searchItem("notebook");
	String result = page.takeScreenshotFromPage(2);
	assertEquals("Screenshot realizado com sucesso!", result);
}

@After
	public void finishDriver()
	{
		Utils.quitDriver();
	}


}
