package com.stepDefinition;

import static org.junit.Assert.assertEquals;

import java.io.FileWriter;

import org.openqa.selenium.By;

import com.test.selenium.Utils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TestSteps {
	/**
	 * Navegar até uma determinada página
	 * 
	 * @param vUrl
	 *            URL da aplicação
	 * @throws Throwable
	 */
	@Given("^Validar a Placa \"([^\"]*)\" com número \"(.*?)\"$")
	public void userValidateCar(String digit, String number) throws Throwable {
		By txtPlacaLetra = By.id("com.devplank.masterplaca:id/et_placa_letra");
		By txtPlacaNumero = By.id("com.devplank.masterplaca:id/et_placa_numero");
		By btnConsultar = By.id("com.devplank.masterplaca:id/fab_consultar");
		
		Utils.driver.findElement(txtPlacaLetra).sendKeys(digit);
		Utils.driver.findElement(txtPlacaNumero).sendKeys(number);
		Utils.driver.findElement(btnConsultar).click();
		
	}

	@Then("^Validar placa existente")
	public void validarPlaca() throws Throwable {
		String result = "Placa não encontraca";
		By lblResultSituacao = By.id("com.devplank.masterplaca:id/result_situacao");
		if (Utils.waitForElementPresent(lblResultSituacao, 30))
		{
			result = "Placa encontrada";
			String details = "";
			String dir = System.getProperty("user.dir");
		    String filename= "Placas.txt";
		    for (int i = 1; i <= 7; i++) {
		    	if (i == 5)
		    	{
		    		details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.TextView[1]")).getText();
	    			details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.LinearLayout/android.widget.TextView[1]")).getText();
	    			details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.LinearLayout/android.widget.TextView[2]")).getText();
	    			details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.LinearLayout/android.widget.TextView[3]")).getText();
		    	}	
		    	else
		    	{
		    		details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.TextView[1]")).getText();
		    		details += Utils.driver.findElement(By.xpath("//android.widget.TableRow[" + i + "]/android.widget.TextView[2]")).getText();
		    	}
		    	details += " | ";
		    }
		    FileWriter fw = new FileWriter(dir + "\\" + filename,true); //the true will append the new data
		    fw.write("\n" + details);//appends the string to the file
		    fw.close();
		}
		
		
		assertEquals("Placa encontrada", result);
	}
	@Then("^Validar placa inexistente")
	public void validarPlacaInexistente() throws Throwable {
		By lblMessage = By.id("android:id/message");
		String result = "Mensagem incorreta";
		String actualResult = "Por favor, verifique se digitou corretamente a placa e tente novamente.";
		if (Utils.waitForElementPresent(By.id("android:id/message"), 30))
			if (Utils.waitForElementTextEquals(lblMessage, actualResult, 30))
					result = Utils.driver.findElement(lblMessage).getText();
		assertEquals(actualResult, result);
	}
	

}