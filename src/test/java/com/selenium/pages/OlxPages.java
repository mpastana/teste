package com.selenium.pages;

import java.io.IOException;

import org.openqa.selenium.By;

import com.test.selenium.Utils;

/**
 * Unit test for simple App.
 */
public class OlxPages {
	// Page Elements
	private By btnCloseAds = By.xpath("//img[@src='http://ced-ns.sascdn.com/diff/templates/images/close_54x54.png']");
	private By txtSearch = By.id("searchtext");
	private By btnSearch = By.id("searchbutton");
	//Functions
	public void login(){
		Utils.driver.navigate().to("http://www.olx.com.br/");
		Utils.driver.findElement(By.id("state-sp")).click();
		if (Utils.isElementPresent(btnCloseAds))
			Utils.driver.findElement(btnCloseAds).click();
	}

	public String searchItems(int maxSearch) {
		int count = 0;
		for (int i = 1; i < maxSearch*2; i++) {
			if (Utils.isElementPresent(By.xpath("//ul[@id='main-ad-list']/li[" + i + "]/a//h3")))
			{
				String title = Utils.driver.findElement(By.xpath("//ul[@id='main-ad-list']/li[" + i + "]/a//h3")).getText();
				String price = "Sem preço";
				if (Utils.isElementPresent(By.xpath("//ul[@id='main-ad-list']/li[" + i + "]/a//p[@class='OLXad-list-price']")))
					price = Utils.driver.findElement(By.xpath("//ul[@id='main-ad-list']/li[" + i + "]/a//p[@class='OLXad-list-price']")).getText();
				System.out.println("Título: " + title + ", Preço: " + price);
				count++;
				if (count == maxSearch)
					return "Encontrado todos os items da lista";
			}
				
		}
		return "Não encontrado todos os items da lista";
	}
	public void searchItem(String item)
	{
		Utils.driver.findElement(txtSearch).sendKeys(item);
		Utils.driver.findElement(btnSearch).click();
	}
	@SuppressWarnings("static-access")
	public String takeScreenshotFromPage(int i) throws IOException, InterruptedException {
		Utils.driver.findElement(By.xpath("//div[@class='module_pagination']//a[text()='" + i +"']")).sendKeys("");	
		Utils.driver.findElement(By.xpath("//div[@class='module_pagination']//a[text()='" + i +"']")).click();	
		Thread.currentThread().sleep(1000);
		String dir = System.getProperty("user.dir");
        Utils.takeScreenshot(dir + "\\print.png");
        return "Screenshot realizado com sucesso!";
	}
}
