package com.apiTest;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**W
 * Unit test for simple App.
 */
public class ApiLibrary 
{
	private String data = "";
	private JsonArray dataArray = new JsonArray();
	private String baseUrl = "";
	private String countryName = "";
	private String countryText = "";
	public void getDataFrom(String url) throws ClientProtocolException, IOException 
	{
		Response response = Request.Get(url)
				.addHeader("Content-Type", "application/json")
				.addHeader("Accept", "application/json")
				.execute();
		data = response.returnContent().toString();
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(data);
		dataArray = element.getAsJsonObject().get("RestResponse").getAsJsonObject().get("result").getAsJsonArray();
	}
	public int countElements() {
		return dataArray.size();
	}
	public boolean IsElementPresent(String country) {
		for (JsonElement jsonElement : dataArray) {
			if (jsonElement.getAsJsonObject().get("name").getAsString().equals(country))
				return true;
		}
		return false;
	}
	public void setCountry(String country) {
		countryName = country;
		
	}
	public void setText(String text) {
		countryText = text;
		
	}
	public void setData() throws ClientProtocolException, IOException {
		Response response = Request.Get(baseUrl + countryName + "?text=" + countryText)
				.addHeader("Content-Type", "application/json")
				.addHeader("Accept", "application/json")
				.execute();
		data = response.returnContent().toString();
	}
	public void setBaseUrl(String url) {
		baseUrl = url;
	}
	public String returnData()
	{
		return data;
	}
}
