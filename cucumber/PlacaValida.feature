#language: pt
# features/my_first.feature

Funcionalidade: Cria um novo registro na agenda e verifica a a criação
Contexto:
  No aplicativo Agenda, acessar e automatizar a criação e a exclusão de um registro com BDD seguindo o seguinte cenário.
  
  Cenario: Verificar registro de um usuário usando a biblioteca Faker para criação dos dados
	
	Dado Validar a Placa "DDD" com número "1331"
	Entao Validar placa existente