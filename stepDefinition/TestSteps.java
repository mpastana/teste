package com.everis.stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.io.File;
import java.util.Set;

import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.Data;
import com.everis.EFA;
import com.everis.Element;
import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataExcel;
@Ignore
public class TestSteps {
	String sc01_baseLocator = "org.openqa.selenium.By.";

	/**
	 * Navegar até uma determinada página
	 * 
	 * @param vUrl
	 *            URL da aplicação
	 * @throws Throwable
	 */
	@Given("^Navegar ate a pagina \"([^\"]*)\"$")
	public void user_Navigate_to_LogIn_Page(String vUrl) throws Throwable {
		EFA.executeAction(Action.Navigate, vUrl);
		if (System.getProperty("platform").equals("WEB-IE"))
			EFA.executeAction(Action.Navigate, "\"javascript:document.getElementById('overridelink').click()" + "\"");
	}

	/**
	 * Preenche um campo identificado por xpath com o valor desejado
	 * 
	 * @param locator
	 *            xpath do campo
	 * @param campo
	 *            valor que será preenchido
	 * @throws Throwable
	 */

	@When("^Preencher o campo \"(.*)\" com o valor \"(.*?)\"$")
	public void sendKeys(String locator, String campo) throws Throwable {
		Element identificador = new Element("xpath", locator);
		EFA.executeAction(Action.SendKeys, identificador, campo);
	}

	/**
	 * Preenche um determinado campo com o valor desejado
	 * 
	 * @param locator
	 *            identificador do campo
	 * @param tipo
	 *            tipo de identificação do campoo
	 * @param campo
	 *            valor que será preenchido
	 * @throws Throwable
	 */

	@When("^Localizar o campo \"(.*?)\" por \"(.*?)\" e preenche-lo com o valor \"(.*?)\"$")
	public void sendKeysWithType(String locator, String tipo, String campo) throws Throwable {
		Element identificador = new Element(tipo, locator);
		EFA.executeAction(Action.SendKeys, identificador, campo);
	}

	/**
	 * Enquanto o campo não estiver preenchido com o valor esperado, o campo é
	 * preenchido
	 * 
	 * @param locator
	 *            xpath do campo
	 * @param campo
	 *            valor que será preenchido
	 * @throws Throwable
	 */
	@When("^Preencher o campo \"(.*)\" com o valor \"(.*?)\" e verificar se esta preenchido$")
	public void inputVerify(String locator, String campo) throws Throwable {
		int count = 0;
		Element identificador = new Element("xpath", locator);

		while (!EFA.executeAction(Action.GetAttribute, identificador, "value").equals(campo) && count <= 10) {
			EFA.executeAction(Action.Clear, identificador);
			EFA.executeAction(Action.SendKeys, identificador, campo);
			count++;
		}
	}

	/**
	 * Enquanto o campo nao estiver preenchido com o valor esperado, o campo e
	 * preenchido atraves de um comando do javascript
	 * 
	 * @param locator
	 *            xpath do campo
	 * @param campo
	 *            valor que sera preenchido
	 * @throws Throwable
	 */
	@When("^Preencher o campo \"(.*)\" com o valor \"(.*?)\" e esperar carregar o valor$")
	public void inputVerifyCep(String locator, String campo) throws Exception {
		int count = 0;
		Element identificador = new Element("xpath", locator);
		//EFA.executeAction(Action.Clear, identificador);
		//EFA.executeAction(Action.JSExecuteScript, "arguments[0].value='" + campo + "';", identificador);
		EFA.cs_executeAction("sendKeysVerify", sc01_baseLocator + "xpath", locator, new String[] { campo });
		while (!EFA.executeAction(Action.GetAttribute, identificador, "value").equals(campo) && count <= 10) {
			EFA.executeAction(Action.Clear, identificador);
			EFA.executeAction(Action.JSExecuteScript, identificador,"arguments[0].value='" + campo + "';");
			count++;
		}
	}
	/**
	 * Enquanto o campo nao obter o valor esperado, o campo e preenchido
	 * 
	 * @param locator
	 *            identificador do campo
	 * @param tipo
	 *            tipo de identificacao do campo
	 * @param campo
	 *            valor que sera preenchido
	 * @throws Throwable
	 */
	@When("^Localizar o campo \"(.*)\" por \"(.*)\", preenche-lo com o valor \"(.*?)\" e verificar se esta preenchido$")
	public void inputVerify(String locator, String tipo, String campo) throws Throwable {
		EFA.cs_executeAction("clear", sc01_baseLocator + tipo, locator, new String[] {});
		EFA.cs_executeAction("sendKeysVerify", sc01_baseLocator + tipo, locator, new String[] { campo });
	}

	/**
	 * Esperar o campo aparecer durante o tempo determinado na variavel
	 * 'cv_testingTries'
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Esperar ate que o objeto \"(.*?)\" apareca$")
	public void clicarnoItemContinuarNesseSite(String locator) throws Throwable {
		//EFA.waitForElement(sc01_baseLocator + "xpath", locator, 20);
		EFA.cf_receiveAction("waitForElementPresent", "xpath", locator, new String[]{"20"});
	}

	/**
	 * Esperar o campo aparecer durante o tempo determinado na variavel
	 * 'cv_testingTries'
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Esperar ate que o objeto \"(.*?)\" nao esteja visivel na tela$")
	public void waitForElementNotVisible(String locator) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;		
		int count = 0;
		while (EFA.isElementPresent(sc01_baseLocator + "xpath", locator)  && count <= 600) {
			Thread.sleep(1000);
			count++;
		}
	}
	
	/**
	 * Esperar o campo aparecer durante o tempo determinado na variavel
	 * 'cv_testingTries'
	 * 
	 * @param locator
	 *            identificador do objeto
	 * @param tipo
	 *            tipo de identificação do campo
	 * @throws Throwable
	 */
	@When("^Localizar o objeto \"(.*?)\" por \"(.*)\" e esperar ate que ele apareca$")
	public void clicarnoItemContinuarNesseSite(String locator, String tipo) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;

		EFA.cf_receiveAction("waitForElementPresent", tipo, locator, new String[]{"10"});
	}

	/**
	 * Clicar em um determinado objeto
	 * 
	 * @param locator
	 *            identificador do objeto
	 * @param tipo
	 *            tipo de identificação do campo
	 * @throws Throwable
	 */
	@When("^Localizar o objeto \"(.*?)\" por \"(.*?)\" e clicar$")
	public void clickOnObject(String locator, String tipo) throws Throwable {
		EFA.cs_executeAction("click", sc01_baseLocator + tipo, locator, new String[] {});
	}

	/**
	 * Clicar em um determinado objeto
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Clicar no objeto \"(.*?)\"$")
	public void clickOnObject(String locator) throws Throwable {
		EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Clicar em um determinado objeto e esperar durante o tempo determinado
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @param segundos
	 *            quantos segundos deve esperar após a ação
	 * @throws Throwable
	 */
	@When("^Clicar no objeto \"(.*?)\" e esperar \"(.*?)\" segundos$")
	public void clickOnObjectAndWait(String locator, String segundos) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		
		EFA.cs_executeAction("jclick", sc01_baseLocator + "xpath", locator, new String[] {});
		Thread.sleep(Integer.parseInt(segundos) * 1000);
	}

	/**
	 * Clicar em um determinado objeto através de um comando JavaScript
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Clicar no objeto \"(.*?)\" com js$")
	public void clickOnObjectJS(String locator) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		int count = 0;
		while (!EFA.cf_receiveAction("isEnabled", sc01_baseLocator + "xpath", locator, new String[] {})
				.equals("true") && count <= 30) {
			count++;
			System.out.println(count);
			Thread.sleep(1000);
		}
		EFA.cs_executeAction("jclick", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Dar foco na tela em um objeto especifico
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Foco no objeto \"(.*?)\"$")
	public void focusOnObject(String locator) throws Throwable {
		
		EFA.cs_executeAction("focus", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Enquanto o objeto não estiver visivel na tela, clicar na tecla TAB ou em
	 * um objeto especifico
	 * 
	 * @param locator
	 *            xpath do objeto que não está visível na tela
	 * @param locator2
	 *            xpath do objeto que deve clicar
	 * @throws Throwable
	 */
	@When("^Enquanto objeto \"(.*)\" nao for visivel, clicar na tecla TAB ou no objeto \"(.*)\"$")
	public void doubleClickOnObject(String locator, String locator2) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		int count = 0;
		while (!EFA.cf_receiveAction("isDisplayed", sc01_baseLocator + "xpath", locator, new String[] {})
				.equals("true") && count <= 50) {
			System.out.println(count);
			if (System.getProperty("platform").equals("WEB-EDGE"))
				EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator2, new String[] {});
			else
				EFA.cs_executeAction("typeTAB", sc01_baseLocator + "xpath", "//body", new String[] {});
			count++;
			Thread.sleep(1000);
		}
	}

	/**
	 * Clicar em um determinado objeto se ele estiver visível na tela
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^Clicar no objeto \"(.*?)\" se visivel$")
	public void clickOnObjectIfVisible(String locator) throws Throwable {
		if (EFA.cf_receiveAction("isDisplayed", sc01_baseLocator + "xpath", locator, new String[] {}).equals("true"))
			EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Clicar em um determinado objeto se ele estiver visível na tela
	 * 
	 * @param locator
	 *            identificador do objeto
	 * @param tipo
	 *            tipo de identificação do objeto
	 * @throws Throwable
	 */
	@When("^Localizar o objeto \"(.*?)\" por \"(.*?)\" e clicar se visivel$")
	public void clickOnObjectIfVisible(String locator, String tipo) throws Throwable {
		if (EFA.isElementPresent(sc01_baseLocator + tipo, locator))
			EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Limpar o valor de um determinado campo
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @throws Throwable
	 */
	@When("^limpar o campo \"(.*?)\"$")
	public void clearField(String locator) throws Throwable {
		EFA.cs_executeAction("clear", sc01_baseLocator + "xpath", locator, new String[] {});
	}

	/**
	 * Limpar o valor de um determinado campo
	 * 
	 * @param locator
	 *            identificador do objeto
	 * @param tipo
	 *            tipo de identificação do objeto
	 * @throws Throwable
	 */
	@When("^limpar o campo \"(.*?)\" identificado por \"(.*)\"$")
	public void clearField(String locator, String tipo) throws Throwable {
		EFA.cs_executeAction("clear", sc01_baseLocator + tipo, locator, new String[] {});
	}

	/**
	 * Capturar o valor de um objeto e sobrescrever o valor existente em um
	 * campo específico do banco
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @param field
	 *            campo do banco onde o valor será sobrescrito
	 * @throws Throwable
	 */
	@When("^Obter o valor do objeto \"(.*?)\" e preencher a coluna \"(.*?)\"$")
	public void getValue(String locator, String field) throws Throwable {
		String output = EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
		
		/*DataExcel dataExcel = openExcel();
		if (!(dataExcel == null)){
			dataExcel.writeCell(Integer.parseInt(GlobalData.getData("index")), field, output);
			dataExcel.save();
			//dataExcel.close();
		}
		*/
		Data data = new Data();
		data.ds_writeData(output, Integer.parseInt(GlobalData.getData("index")), field);
	}

	/**
	 * Capturar o valor do objeto e acrescentar esse valor em um campo
	 * específico do banco
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @param field
	 *            campo do banco onde o valor será acrescentado
	 * @throws Throwable
	 */
	@When("^Obter o valor do objeto \"(.*?)\" e adicionar a coluna \"(.*?)\"$")
	public void getValueAndAdd(String locator, String field) throws Throwable {
		/*DataExcel dataExcel = openExcel();
		
		String old = dataExcel.readCell(Integer.parseInt(GlobalData.getData("index")), field);
		old += EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
		dataExcel.writeCell(Integer.parseInt(GlobalData.getData("index")), field, old);
		dataExcel.save();
		//dataExcel.close();
		*/
		Data data = new Data();
		String old = data.df_getData(Integer.parseInt(GlobalData.getData("index")), field);
		old += EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
		data.ds_writeData(old, Integer.parseInt(GlobalData.getData("index")), field);
	}
	
	@When("^Localizar o objeto \"(.*?)\" por \"(.*?)\", obter o valor e preencher a coluna \"(.*?)\"$")
	public void getValueAndAdd(String locator, String type, String field) throws Throwable {
		//DataExcel dataExcel = openExcel();
		
		String text = "";
		text = EFA.cf_receiveAction("getText", sc01_baseLocator + type, locator, new String[] {});
		/*dataExcel.writeCell(Integer.parseInt(GlobalData.getData("index")), field, text);
		dataExcel.save();
		*/
		Data data = new Data();
		data.ds_writeData(text, Integer.parseInt(GlobalData.getData("index")), field);
		
		GlobalData.setData(field, text);
	}

	/**
	 * Sobrescrever o valor de um determinado campo do banco
	 * 
	 * @param col
	 *            campo do banco onde o valor será sobrescrito
	 * @param value
	 *            valor que será inserido no banco
	 * @throws Throwable
	 */
	@When("^Preencher a coluna \"(.*?)\" com o valor \"(.*?)\"$")
	public void makeVariable(String col, String value) throws Throwable {
		/*DataExcel dataExcel = openExcel();
		
		dataExcel.writeCell(Integer.parseInt(GlobalData.getData("index")), col, value);
		dataExcel.save();
		*/
		Data data = new Data();
		data.ds_writeData(value, Integer.parseInt(GlobalData.getData("index")), col);
	}

	/**
	 * Acrescentar um valor de um determinado campo do banco
	 * 
	 * @param col
	 *            campo do banco onde o valor será acrescentado
	 * @param value
	 *            valor que será acrescentado
	 * @throws Throwable
	 */
	@When("^Adicionar a coluna \"(.*?)\" o valor \"(.*?)\"$")
	public void makeVariableAndAdd(String col, String value) throws Throwable {
		/*DataExcel dataExcel = openExcel();
		
		String old =dataExcel.readCell(Integer.parseInt(GlobalData.getData("index")), col);
		old += value;
		dataExcel.writeCell(Integer.parseInt(GlobalData.getData("index")), col, old);
		*/
		Data data = new Data();
		String old = data.df_getData(Integer.parseInt(GlobalData.getData("index")), col);
		old += value;
		data.ds_writeData(old, Integer.parseInt(GlobalData.getData("index")), col);
	}

	/**
	 * Trocar para um determinado Frame da aplicação
	 * 
	 * @param frameName
	 *            nome do frame
	 * @throws Throwable
	 */
	@When("^Alterar para o frame \"(.*?)\"$")
	public void alterToFrame(String frameName) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		try {
			WebElement iframe = EFA.cv_driver.findElement(By.xpath(frameName));
			EFA.cv_driver.switchTo().frame(iframe);
		} catch (Exception e) {
			e.printStackTrace();
			EFA.cv_onError = e.getMessage();
		}

	}


	/**
	 * Avançar na tela de erro de certificado
	 * 
	 * @param locator
	 *            ID do objeto
	 * @param type
	 *            tipo de identificação do objeto
	 * @param tempo
	 *            tempo máximo que deverá esperar até o erro de certificado
	 *            aparecer
	 * @throws Throwable
	 */
	@When("^Verificar se o certificado \"(.*?)\" identificado por \"(.*?)\" existe durante \"(.*?)\" segundos$")
	public void user_verifyCetificate(String locator, String type, String tempo) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		if (System.getProperty("platform").equals("WEB-IE") || System.getProperty("platform").equals("WEB-EDGE"))
			EFA.cs_executeAction("navigate", sc01_baseLocator + "xpath", "",
					new String[] { "\"" + "javascript:document.getElementById('" + locator + "').click()" + "\"" });
	}

	/**
	 * Verificar se o resultado encontrado é o resultado esperado
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	@When("^Verificar o resultado esperado")
	public void checkValue() throws NumberFormatException, Exception {
		if (!EFA.cv_onError.equals(""))
			return;
		Thread.sleep(1000);
		Data data = new Data();
		String output = data.df_getData(Integer.parseInt(GlobalData.getData("index")), "Expected Result");
		EFA.cf_checkValue(Integer.parseInt(GlobalData.getData("index")), output , data);
	}

	/**
	 * Verificar se o objeto esperado existe na tela e inserir o resultado
	 * encontrado
	 * 
	 * @param campo
	 *            objeto que será verificado
	 * @param coluna
	 *            campo do banco onde o resultado encontrado será inserido
	 * @throws Exception 
	 * @throws NumberFormatException 
	 */
	@When("^Verificar se o objeto \"(.*?)\" existe e incluir o resultado na coluna \"(.*?)\"$")
	public void exists(String campo, String coluna) throws NumberFormatException, Exception {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		boolean exists = Boolean.parseBoolean(EFA.cf_receiveAction("waitForElementPresent", "xpath", campo, new String[]{"15"}));
		String resultadoEsperado = "";
		resultadoEsperado = GlobalData.getData("Expected Result");
		if (exists) {
			ExecutionInfo.setResult("teste efetuado");
		} else
			ExecutionInfo.setResult("Houve um erro");
	}
	
	@When("^Verificar se o valor da coluna \"(.*?)\" e igual ao da coluna \"(.*?)\"$")
	public void valueIsEqual(String coluna1, String coluna2 ) {
		if (!EFA.cv_onError.equals("")) {
			return;
		}

		String resultadoEsperado = "";
		resultadoEsperado = GlobalData.getData("Expected Result");
		System.out.println(GlobalData.getData(coluna1));
		System.out.println(GlobalData.getData(coluna2));
		if (GlobalData.getData(coluna1).equals(GlobalData.getData(coluna2))) {
			ExecutionInfo.setResult(resultadoEsperado);
		} else
			ExecutionInfo.setResult("Valores diferentes");
	}

	/**
	 * Selecionar um determinado valor em um determinado combo
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @param valor
	 *            valor que será selecionado no combo
	 * @throws Throwable
	 */
	@When("^Selecionar no campo \"(.*?)\" o valor \"(.*?)\"$")
	public void selecionarTipoSeguro(String locator, String valor) throws Throwable {
		EFA.cs_executeAction("selectByVisibleText", sc01_baseLocator + "xpath", locator, new String[] { valor });
	}

	/**
	 * Selecionar um valor em um determinado combo e esperar durante o tempo
	 * determinado
	 * 
	 * @param locator
	 *            xpath do objeto
	 * @param valor
	 *            valor que será selecionado no combo
	 * @param segundos
	 *            quantos segundos deve esperar depois de efetuar a ação
	 * @throws Throwable
	 */
	@When("^Selecionar no campo \"(.*?)\" o valor \"(.*?)\" e esperar \"(.*?)\" segundos$")
	public void selecionarTipoSeguroEEsperar(String locator, String valor, String segundos) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		EFA.cs_executeAction("selectByVisibleText", sc01_baseLocator + "xpath", locator, new String[] { valor });
		Thread.sleep(Integer.parseInt(segundos) * 1000);
	}

	/**
	 * Selecionar um valor em um determinado combo
	 * 
	 * @param locator
	 *            identificador do objeto
	 * @param tipo
	 *            tipo de identificação do objeto
	 * @param valor
	 *            valor que será selecionado
	 * @throws Throwable
	 */
	@When("^Localizar o campo \"(.*?)\" por \"(.*?)\" e selecionar o valor \"(.*?)\"$")
	public void selecionarTipoSeguro(String locator, String tipo, String valor) throws Throwable {
		EFA.cs_executeAction("selectByVisibleText", sc01_baseLocator + tipo, locator, new String[] { valor });
	}

	/**
	 * Enquanto um determinado objeto não existir, clicar em um outro objeto
	 * 
	 * @param locator
	 *            xpath do objeto que está esperando
	 * @param locator2
	 *            xpath do objeto que será clicado
	 * @throws Throwable
	 */
	/*@When("^Enquanto objeto \"(.*)\" nao existir, clicar no objeto \"(.*?)\"$")
	public void clicar_no_bot_o_ProximoCOB(String locator, String locator2) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		int count = 0;
		while (!EFA.isElementPresent(sc01_baseLocator + "xpath", locator) && count <= 15) {
			if (EFA.isElementPresent(sc01_baseLocator + "xpath", locator2))
				EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator2, new String[] {});
			count++;
			Thread.sleep(1000);
		}
		if (count > 15)
			EFA.cv_onError = "Error on object " + locator + ": Object not exist after click in " + locator2;
	}*/

	/**
	 * Enquanto o objeto esperado não existir, clicar em um campo, clicar fora
	 * desse campo e clicar em um botao
	 * 
	 * @param locator
	 *            xpath do objeto esperado
	 * @param label
	 *            xpath do objeto que será clicado para tirar o foco do campo
	 * @param campo
	 *            xpath do objeto que será clicado
	 * @param locator2
	 *            xpath do botão que será clicado
	 * @throws Throwable
	 */
	@When("^Enquanto objeto \"(.*)\" nao existir, clicar no label \"(.*?)\", clicar no campo \"(.*?)\" e clicar no objeto \"(.*?)\"$")
	public void whileNotExistsClick(String locator, String label, String campo, String locator2) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		int count = 0;
		while (!EFA.isElementPresent(sc01_baseLocator + "xpath", locator) && count <= 15) {
			EFA.cs_executeAction("click", sc01_baseLocator + "xpath", campo, new String[] {});
			EFA.cs_executeAction("click", sc01_baseLocator + "xpath", label, new String[] {});
			EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator2, new String[] {});
			count++;
			Thread.sleep(1000);
		}
		if (count > 15)
			EFA.cv_onError = "Error on object " + locator + ": Object not exist after click in " + campo + ", " + label + " and " + locator2;
	}

	/**
	 * Enquanto o objeto esperado não existir, clicar em um determinado objeto
	 * 
	 * @param locator
	 *            identificador do objeto esperado
	 * @param tipo
	 *            tipo de identificação do objeto esperado
	 * @param locator2
	 *            identificador do objeto
	 * @param tipo2
	 *            tipo de identificação do objeto
	 * @throws Throwable
	 */
	@When("^Enquanto objeto \"(.*)\" identificado por \"(.*)\" nao existir, clicar no objeto \"(.*?)\" identificado por \"(.*)\"$")
	public void clicar_no_bot_o_ProximoCOB(String locator, String tipo, String locator2, String tipo2)
			throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		int count = 0;
		while (!EFA.isElementPresent(tipo, locator) && count <= 15) {
			if (EFA.isElementPresent(tipo2, locator2))
				EFA.cs_executeAction("click", sc01_baseLocator + tipo2, locator2, new String[] {});
			count++;
			Thread.sleep(1000);
		}
		if (count > 15)
			EFA.cv_onError = "Error on object " + locator + ": Object not exist after click in " + locator2;
	}

	/**
	 * Esperar até que dois objetos estejam visíveis na tela
	 * 
	 * @param formulario
	 *            xpath do objeto esperado
	 * @param locator
	 *            xpath do segundo objeto esperado
	 * @throws Throwable
	 */
	@Given("^Esperar ate formulario \"(.*)\" e o campo \"(.*)\" carregar$")
	public void clicar_no_campo_Segurado_Principal_Condutor(String formulario, String locator) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		int count = 0;
		EFA.cs_executeAction("click", sc01_baseLocator + "xpath", locator, new String[] {});
		String fieldValue = EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
		do {
			Thread.sleep(1000);
			count++;
		} while (fieldValue == "" && count <= 15);
	}

	/**
	 * Tirar evidência da tela com um determinado nome
	 * 
	 * @param nome
	 *            nome da evidência
	 * @throws Throwable
	 */
	@When("^Tirar evidencia da tela com o nome \"(.*)\"$")
	public void print(String nome) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		
		EFA.cf_getTestEvidenceWithStep(nome, 0);
	}

	/**
	 * Trocar de janela
	 * 
	 * @param numeroJanela
	 *            numero da janela para o qual deseja trocar (1- primeiro, 2-
	 *            segundo, etc)
	 * @throws Throwable
	 */
	@When("^Trocar para a janela numero \"(.*)\"$")
	public void switchWindow(String numeroJanela) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		Set<String> janelas;
		janelas = EFA.cv_driver.getWindowHandles();
		int count = 1;
		for (String janela : janelas) {
			if (count == Integer.parseInt(numeroJanela))
				EFA.cv_driver.switchTo().window(janela);
			count++;
		}
	}

	/**
	 * Esperar durante o tempo determinado
	 * 
	 * @param segundos
	 *            quantos segundos deve esperar
	 * @throws Throwable
	 */
	@When("^Esperar durante \"(.*?)\" segundos$")
	public void wait(String segundos) throws Throwable {
		if (!EFA.cv_onError.equals("")) {
			return;
		}
		int tempo = Integer.parseInt(segundos) * 1000;
		Thread.sleep(tempo);
	}

	/**
	 * Esperar até que o campo carregue o valor com todas as letras em maiúsculo
	 * 
	 * @param locator
	 *            xpath do campo
	 * @throws Throwable
	 */
	@When("^Esperar esperar ate o campo \"(.*?)\" carregar o valor correto$")
	public void waitForValue(String locator) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		int count = 0;
		while (count <= 6) {
			String value = EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
			if (value.equals(value.toUpperCase()))
				break;
			count++;
			Thread.sleep(1000);
		}
	}

	/**
	 * Esperar até o campo carregar algum valor
	 * 
	 * @param locator identificador do objeto
	 * @throws Throwable
	 */
	@When("^Esperar enquanto o campo \"(.*?)\" estiver vazio")
	public void waitForValueLoad(String locator) throws Throwable {
		if (!EFA.cv_onError.equals(""))
			return;
		int count = 0;
		while (count <= 10) {
			String value = EFA.cf_receiveAction("getText", sc01_baseLocator + "xpath", locator, new String[] {});
			if (!value.equals(""))
				break;
			count++;
			Thread.sleep(1000);
		}
	}
	
	@When("^Trocar para o frame \"(.*?)\"$")
	public void switchFrame(String name){
		if (!EFA.cv_onError.equals(""))
			return;
		EFA.cv_driver.switchTo().frame(name);
	}
	
	public DataExcel openExcel(){
		DataExcel excel = new DataExcel(
				new File("data").getAbsoluteFile() + "/" + ExecutionInfo.getTestSuite() + ".xlsx");
		try {
			excel.open();
			excel.readSheet(ExecutionInfo.getTestName());
			return excel;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			EFA.cv_onError = e.getMessage();
			return null;
		}
	}
}